package eti.asw.accelerometer.accelerometer;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import static android.graphics.Color.rgb;

public class AccelerometerDataActivity extends AppCompatActivity implements Observer {

    public String read = "";
    public void onShowRecordedButton(View view) {
        Intent intent = new Intent(this, RecordedActivity.class);

        startActivity(intent);
    }

    private class ValuesXYZ{
        float x;
        float y;
        float z;
        public ValuesXYZ(float x, float y, float z){
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
    /*private class ReadValues{
        ValuesXYZ acc;
        ValuesXYZ calcAcc;
        ValuesXYZ grav;
        public ReadValues(ValuesXYZ acc,
                          ValuesXYZ calcAcc,
                          ValuesXYZ grav)
        {
            this.acc = acc ;
            this.calcAcc = calcAcc;
            this.grav=grav;
        }

    }*/
    private List<ValuesXYZ> accList;
    private List<ValuesXYZ> calcList;
    private List<ValuesXYZ> gravList;
    private AccelerometerData accelerometerData;
    private boolean recording;
    private Button recordButton;
    private Drawable recordButtonBackgoundBase;
    private final int COLOR_T = rgb(255,0,0);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelerometer_data);
        accList = new ArrayList<>();
        calcList = new ArrayList<>();
        gravList = new ArrayList<>();
        accelerometerData = new AccelerometerData(this);
        accelerometerData.addObserver(this);
        recording = false;
        recordButton = (Button) findViewById(R.id.recordButton);
        recordButtonBackgoundBase = recordButton.getBackground();
        recordButton.setBackground(recordButtonBackgoundBase);
        //recordButton.setBackgroundColor(COLOR_F);

    }



    private void refreshAccelerometerTextView(){
        TextView accelerometerXAxisTextView = (TextView) findViewById(R.id.accelerometerXAxisTextView);
        TextView accelerometerYAxisTextView = (TextView) findViewById(R.id.accelerometerYAxisTextView);
        TextView accelerometerZAxisTextView = (TextView) findViewById(R.id.accelerometerZAxisTextView);

        float[] data = accelerometerData.getLastAcceleration();
        float xAxis = data[0];
        float yAxis = data[1];
        float zAxis = data[2];

        accelerometerXAxisTextView.setText("x axis: " +String.format("%f", xAxis));
        accelerometerYAxisTextView.setText("y axis: " +String.format("%f", yAxis));
        accelerometerZAxisTextView.setText("z axis: " +String.format("%f", zAxis));

        if(recording) accList.add(new ValuesXYZ(xAxis,yAxis,zAxis));

    }

    private void refreshLinearAccelerationTextView(){
        TextView linearAccerometerXAxisTextView = (TextView) findViewById(R.id.linearAccelerationXAxisTextView);
        TextView linearAccerometerYAxisTextView = (TextView) findViewById(R.id.linearAccelerationYAxisTextView);
        TextView linearAccerometerZAxisTextView = (TextView) findViewById(R.id.linearAccelerationZAxisTextView);

        float[] data = accelerometerData.getLastLinearAcceleration();
        float xAxis = data[0];
        float yAxis = data[1];
        float zAxis = data[2];

        if (accelerometerData.getIsLinearAccelerationCalculate()){
            linearAccerometerXAxisTextView.setText("x axis(calculated): " +String.format("%f", xAxis));
            linearAccerometerYAxisTextView.setText("y axis(calculated): " +String.format("%f", yAxis));
            linearAccerometerZAxisTextView.setText("z axis(calculated): " +String.format("%f", zAxis));
        }
        else{
            linearAccerometerXAxisTextView.setText("x axis: "+String.format("%f", xAxis));
            linearAccerometerYAxisTextView.setText("y axis: "+String.format("%f", yAxis));
            linearAccerometerZAxisTextView.setText("z axis: "+String.format("%f", zAxis));
        }
        if(recording) calcList.add(new ValuesXYZ(xAxis,yAxis,zAxis));
    }

    private void refreshGravityTextView(){
        TextView gravityXAxisTextView = (TextView) findViewById(R.id.gravityXAxisTextView);
        TextView gravityYAxisTextView = (TextView) findViewById(R.id.gravityYAxisTextView);
        TextView gravityZAxisTextView = (TextView) findViewById(R.id.gravityZAxisTextView);

        float[] data = accelerometerData.getLastGravity();
        float xAxis = data[0];
        float yAxis = data[1];
        float zAxis = data[2];

        if (accelerometerData.getIsGravityCalculate()){
            gravityXAxisTextView.setText("x axis(calculated): " +String.format("%f", xAxis));
            gravityYAxisTextView.setText("y axis(calculated): " +String.format("%f", yAxis));
            gravityZAxisTextView.setText("z axis(calculated): " +String.format("%f", zAxis));
        }
        else{
            gravityXAxisTextView.setText("x axis: "+String.format("%f", xAxis));
            gravityYAxisTextView.setText("y axis: "+String.format("%f", yAxis));
            gravityZAxisTextView.setText("z axis: "+String.format("%f", zAxis));
        }
        if(recording) gravList.add(new ValuesXYZ(xAxis,yAxis,zAxis));
    }


    public void onReturnButton(View view){
        finish();
        accelerometerData.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        accelerometerData.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        accelerometerData.pause();
    }

    @Override
    public void update(Observable observable, Object o) {
        refreshAccelerometerTextView();
        refreshLinearAccelerationTextView();
        refreshGravityTextView();
    }
    public void onRecordButton(View view) {

        if (!recording)
        {
            recording = true;
            recordButton.setBackgroundColor(COLOR_T);
        }
        else
        {
            recording = false;
            recordButton.setBackground(recordButtonBackgoundBase);
            saveToFile();
        }

    }
    public void saveToFile()
    {
        String filename = "recorded";
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());
        String string = "Generated: "+currentDateandTime+"\n\n\n";
        for (int i =0 ;i< accList.size();i++) {
            string += ">acc \tx: "+String.format("%.3f",accList.get(i).x) + "\t\t\ty: "
                    + String.format("%.3f", accList.get(i).y) + "\t\t\t\tz:"
                    + String.format("%.3f",accList.get(i).z) + "\n>line \tx: "
                    + String.format("%.3f",calcList.get(i).x) + "\t\t\ty: "
                    + String.format("%.3f",calcList.get(i).y) + "\t\t\t\tz: "
                    + String.format("%.3f",calcList.get(i).z) + "\n>grav \tx: "
                    + String.format("%.3f",gravList.get(i).x) + "\t\t\ty: "
                    + String.format("%.3f",gravList.get(i).y) + "\t\t\t\tz: "
                    + String.format("%.3f",gravList.get(i).z) + "\n----------------------------------------------------------------\n";
        }
        deleteFile(filename);
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(string.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
