package eti.asw.accelerometer.accelerometer;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onSensorsDataButton(View view){
        Intent intent = new Intent(this, AccelerometerDataActivity.class);
        startActivity(intent);
    }

    public void onVelocityButton(View view){
        Intent intent = new Intent(this, VelocityActivity.class);
        startActivity(intent);
    }

    public void graphPresentation(View view){
        Intent intent = new Intent(this, Graph.class);

        startActivity(intent);
    }

    public void aboutButton(View view){
        Intent intent = new Intent(this, About.class);
        startActivity(intent);
    }

    public void ballButton(View view) {
        Intent intent = new Intent(this, Ball.class);
        startActivity(intent);
    }

    public void onDisplacementButton(View view) {
        Intent intent = new Intent(this, DisplacementActivity.class);
        startActivity(intent);
    }
}
