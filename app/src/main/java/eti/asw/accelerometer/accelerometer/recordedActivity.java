package eti.asw.accelerometer.accelerometer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class RecordedActivity extends AppCompatActivity {

    private String text="";
    /*public RecordedActivity(String text)
    {
        this.text = text;
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorded);
        TextView recordedTextView = (TextView) findViewById(R.id.recTextView);
        recordedTextView.setMovementMethod(new ScrollingMovementMethod());
        File filesDir = getFilesDir();

        try {
            Scanner input = new Scanner(new File(filesDir, "recorded"));
            while (input.hasNext())
            {
                text += input.nextLine()+"\n";
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        recordedTextView.setText(text);
    }


}
