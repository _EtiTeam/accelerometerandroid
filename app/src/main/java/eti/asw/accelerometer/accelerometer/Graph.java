package eti.asw.accelerometer.accelerometer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Observable;
import java.util.Observer;

public class Graph extends AppCompatActivity implements Observer {

    private AccelerometerData accelerometerData;

    private LineGraphSeries<DataPoint> series;
    private LineGraphSeries<DataPoint> series2;
    private LineGraphSeries<DataPoint> series3;
    private int lastX = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        //sensors initialize
        accelerometerData = new AccelerometerData(this);
        accelerometerData.addObserver(this);

        int rangeYMax = 25;
        int rangeYMin = -25;

        //graph settings
        GraphView graph = (GraphView) findViewById(R.id.graph);
        series = new LineGraphSeries<DataPoint>();
        graph.addSeries(series);
        Viewport viewport = graph.getViewport();
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(rangeYMin);
        viewport.setMaxY(rangeYMax);
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(10);
        viewport.setMaxX(100);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);


        GraphView graph2 = (GraphView) findViewById(R.id.graph2);
        series2 = new LineGraphSeries<DataPoint>();
        graph2.addSeries(series2);
        Viewport viewport2 = graph2.getViewport();
        viewport2.setYAxisBoundsManual(true);
        viewport2.setMinY(rangeYMin);
        viewport2.setMaxY(rangeYMax);
        viewport2.setXAxisBoundsManual(true);
        viewport2.setMinX(10);
        viewport2.setMaxX(100);
        graph2.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        
        GraphView graph3 = (GraphView) findViewById(R.id.graph3);
        series3 = new LineGraphSeries<DataPoint>();
        graph3.addSeries(series3);
        Viewport viewport3 = graph3.getViewport();
        viewport3.setYAxisBoundsManual(true);
        viewport3.setMinY(rangeYMin);
        viewport3.setMaxY(rangeYMax);
        viewport3.setXAxisBoundsManual(true);
        viewport3.setMinX(10);
        viewport3.setMaxX(100);
        graph3.getGridLabelRenderer().setHorizontalLabelsVisible(false);

        //viewport.setScrollable(true);

    }

    @Override
    protected void onPause() {
        super.onPause();
        accelerometerData.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        accelerometerData.resume();
    }

    @Override
    public void update(Observable observable, Object o) {
        float[] data = accelerometerData.getLastAcceleration();
        //double[] data = accelerometerData.getLastVelocity();
        float xAxis = (float)data[0];
        float yAxis = (float)data[1];
        float zAxis = (float)data[2];
        series.appendData(new DataPoint(lastX++, xAxis), true, 100);
        series2.appendData(new DataPoint(lastX++, yAxis), true, 100);
        series3.appendData(new DataPoint(lastX++, zAxis), true, 100);
    }
}
