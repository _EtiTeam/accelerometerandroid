package eti.asw.accelerometer.accelerometer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Observable;
import java.util.Observer;

import static java.lang.StrictMath.abs;

public class VelocityActivity extends AppCompatActivity implements Observer {

    private AccelerometerData accelerometerData;

    private LineGraphSeries<DataPoint> series;
    private LineGraphSeries<DataPoint> series2;
    private LineGraphSeries<DataPoint> series3;
    private int lastX = 0;
    double xMAX = 0.0;
    double yMAX = 0.0;
    double zMAX = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_velocity);

        accelerometerData = new AccelerometerData(this);
        accelerometerData.addObserver(this);

        int rangeYMax = 1;
        int rangeYMin = -1;

        //graph settings
        GraphView graph = (GraphView) findViewById(R.id.velocityXGraph);
        series = new LineGraphSeries<DataPoint>();
        graph.addSeries(series);
        Viewport viewport = graph.getViewport();
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(rangeYMin);
        viewport.setMaxY(rangeYMax);
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(10);
        viewport.setMaxX(100);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);


        GraphView graph2 = (GraphView) findViewById(R.id.velocityYGraph);
        series2 = new LineGraphSeries<DataPoint>();
        graph2.addSeries(series2);
        Viewport viewport2 = graph2.getViewport();
        viewport2.setYAxisBoundsManual(true);
        viewport2.setMinY(rangeYMin);
        viewport2.setMaxY(rangeYMax);
        viewport2.setXAxisBoundsManual(true);
        viewport2.setMinX(10);
        viewport2.setMaxX(100);
        graph2.getGridLabelRenderer().setHorizontalLabelsVisible(false);

        GraphView graph3 = (GraphView) findViewById(R.id.velocityZGraph);
        series3 = new LineGraphSeries<DataPoint>();
        graph3.addSeries(series3);
        Viewport viewport3 = graph3.getViewport();
        viewport3.setYAxisBoundsManual(true);
        viewport3.setMinY(rangeYMin);
        viewport3.setMaxY(rangeYMax);
        viewport3.setXAxisBoundsManual(true);
        viewport3.setMinX(10);
        viewport3.setMaxX(100);
        graph3.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        accelerometerData.resetVelocity();

    }

    private void refreshVelocityTextView(){
        TextView velocityXAxis = (TextView) findViewById(R.id.velocityXAxis);
        TextView velocityYAxis = (TextView) findViewById(R.id.velocityYAxis);
        TextView velocityZAxis = (TextView) findViewById(R.id.velocityZAxis);
       // TextView velocityZAxis = (TextView) findViewById(R.id.velocityZAxis);

        double[] data = accelerometerData.getLastVelocity();
        double xAxis = data[0];
        double yAxis = data[1];
        double zAxis = data[2];
        if (abs(xAxis)<0.05) xAxis = 0.0;
        if (abs(yAxis)<0.05) yAxis = 0.0;
        if (abs(zAxis)<0.05) zAxis = 0.0;
        if (abs(xAxis)>abs(xMAX)) xMAX = xAxis;
        if (abs(yAxis)>abs(yMAX)) yMAX = yAxis;
        if (abs(zAxis)>abs(zMAX)) zMAX = zAxis;

        velocityXAxis.setText("x axis[m/s]: " + String.format("%f", xAxis)+ "\t max: "+String.format("%f", xMAX));
        velocityYAxis.setText("y axis[m/s]: " + String.format("%f", yAxis)+ "\t max: "+String.format("%f", yMAX));
        velocityZAxis.setText("z axis[m/s]: " + String.format("%f", zAxis)+ "\t max: "+String.format("%f", zMAX));
    }

    private void refreshVelocityGraph(){
        double[] data = accelerometerData.getLastVelocity();
        double xAxis = data[0];
        double yAxis = data[1];
        double zAxis = data[2];

        series.appendData(new DataPoint(lastX++, xAxis), true, 100);
        series2.appendData(new DataPoint(lastX++, yAxis), true, 100);
        series3.appendData(new DataPoint(lastX++, zAxis), true, 100);
    }

    public void onReturnButton(View view){
        finish();
        accelerometerData.pause();
    }

    public void onResetVelocityButton(View view){
        accelerometerData.resetVelocity();
        xMAX =0;
        yMAX =0;
        zMAX =0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        accelerometerData.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        accelerometerData.pause();
    }

    @Override
    public void update(Observable observable, Object o) {
        refreshVelocityTextView();
        refreshVelocityGraph();
    }
}
