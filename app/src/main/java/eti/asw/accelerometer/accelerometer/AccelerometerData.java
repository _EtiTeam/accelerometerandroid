package eti.asw.accelerometer.accelerometer;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.Date;
import java.util.Observable;
import java.util.Timer;

import static java.lang.Math.abs;

public class AccelerometerData extends Observable implements SensorEventListener{

    private SensorManager mSensorManager;

    private Sensor mAccelerometer;
    private Sensor mLinearAcceleration;
    private Sensor mGravity;

    private Clock lastClock;
    private Clock clock;

    private float lastAcceleration[] = new float[3];
    private float previousAcceleration[] = new float[15];

    private float lastGravity[] = new float[3];

    private float lastLinearAcceleration[] = new float[3];
    private float previousLinearAcceleration[] = new float[3]; // before lastLinearAcceleration

    private double lastVelocity[] = new double[3];
    private double previousVelocity[] = new double[3];

    private double lastDisplacement[] = new double[3];

    private boolean isGravityCalculate = false;             // default value it is changed after first calculation
    private boolean isLinearAccelerationCalculate = false;  // default value it is changed after first calculation

    private boolean isRegistered = false;


    private int delayTime = SensorManager.SENSOR_DELAY_GAME;
    private float alpha = 0.977f;

    public AccelerometerData(Context context){
        super();
        // https://developer.android.com/guide/topics/sensors/sensors_motion.html
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

        mLinearAcceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mGravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        lastClock = new Clock();
        clock = new Clock();
        resetVelocity();
    }

    public void setDelayTime(int delayTime){
        this.delayTime = delayTime;
        if (isRegistered){
            pause();
            resume();
        }
    }

    public int getDelayTime(){
        return delayTime;
    }

    public void resume(){
        clock.reset();

        registerListeners(delayTime);
        isRegistered = true;
    }

    public void pause(){
        mSensorManager.unregisterListener(this);
        isRegistered = false;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        clock.stop();
        lastClock = clock;
        clock = new Clock();


        // shift old data
        System.arraycopy(previousAcceleration, 0, previousAcceleration, 3, 12);
        //System.arraycopy(previousLinearAcceleration, 0, previousLinearAcceleration, 3, 12);
        //System.arraycopy(previousVelocity, 0, previousVelocity, 3, 12);
        // 'backup' last data
        System.arraycopy(lastAcceleration, 0, previousAcceleration, 0, 3);
        System.arraycopy(lastLinearAcceleration, 0, previousLinearAcceleration, 0, 3);
        System.arraycopy(lastVelocity, 0, previousVelocity, 0, 3);



        // view for current sensor
        float xAxis = event.values[0];
        float yAxis = event.values[1];
        float zAxis = event.values[2];

        if (event.sensor == mAccelerometer) {
            lastAcceleration[0] = xAxis;
            lastAcceleration[1] = yAxis;
            lastAcceleration[2] = zAxis;

            // https://developer.android.com/guide/topics/sensors/sensors_motion.html#sensors-motion-accel

            // if linearAcceleration and Gravity isn't provided by hardware
            if (mLinearAcceleration == null && mGravity == null){
                countAverageAcceleration();
                calculateGravityAndLinearAcceleration(xAxis, yAxis, zAxis);

                isLinearAccelerationCalculate = true;
                isGravityCalculate = true;
            }
        }
        else if (event.sensor == mLinearAcceleration) {
            lastLinearAcceleration[0] = xAxis;
            lastLinearAcceleration[1] = yAxis;
            lastLinearAcceleration[2] = zAxis;

            isLinearAccelerationCalculate = false;
        }
        else if (event.sensor == mGravity) {
            lastGravity[0] = xAxis;
            lastGravity[1] = yAxis;
            lastGravity[2] = zAxis;
            isGravityCalculate = false;
        }
        else{
            // prevent notify
            return;
        }

        calculateVelocity();
        calculateDisplacement();
        setChanged();
        notifyObservers();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void countAverageAcceleration(){
        float average;
        for (int i = 0; i < 3; ++i) { // for 3 axis
            average = lastAcceleration[i];

            // for previous data
            final int PREVIOUS_DATA = 5;
            for(int j = 0; j < PREVIOUS_DATA; ++j){
                average += previousAcceleration[j*3 + i];
            }
            average /= PREVIOUS_DATA + 1;
            lastAcceleration[i] = average;
        }

    }

    private void registerListeners(int sensorDelay){
        mSensorManager.registerListener(this, mLinearAcceleration, sensorDelay);
        mSensorManager.registerListener(this, mGravity, sensorDelay);
        mSensorManager.registerListener(this, mAccelerometer, sensorDelay);
    }

    public float[] getLastAcceleration(){
        return lastAcceleration;
    }

    public float[] getLastGravity(){
        return lastGravity;
    }

    public float[] getLastLinearAcceleration(){
        return lastLinearAcceleration;
    }

    public boolean getIsGravityCalculate(){
        return isGravityCalculate;
    }

    public boolean getIsLinearAccelerationCalculate(){
        return isLinearAccelerationCalculate;
    }


    private void calculateGravityAndLinearAcceleration(float accelerometerXAxis, float accelerometerYAxis, float accelerometerZAxis){
        // Isolate the force of gravity with the low-pass filter.
        lastGravity[0] = alpha * lastGravity[0] + (1 - alpha) * accelerometerXAxis;
        lastGravity[1] = alpha * lastGravity[1] + (1 - alpha) * accelerometerYAxis;
        lastGravity[2] = alpha * lastGravity[2] + (1 - alpha) * accelerometerZAxis;

        // Remove the gravity contribution with the high-pass filter.
        lastLinearAcceleration[0] = accelerometerXAxis - lastGravity[0];
        lastLinearAcceleration[1] = accelerometerYAxis - lastGravity[1];
        lastLinearAcceleration[2] = accelerometerZAxis - lastGravity[2];

        /*
        // progowanie
        final float minValue = 0.1f;
        for (int i = 0; i < 3; ++i){
            if (abs(lastLinearAcceleration[i]) < abs(minValue))
                lastLinearAcceleration[i] = 0;
        }
        */

        isGravityCalculate = true;
        isLinearAccelerationCalculate = true;
    }

    private void calculateVelocity(){
        double seconds = lastClock.getSeconds();
        double c1,c2,c3;

        for (int i = 0; i < 3; ++i) { // for 3 axis

            c1 = (double) (previousLinearAcceleration[i] + lastLinearAcceleration[i]);
            c2 = c1 * seconds;
            c3 = c2 / 2.0;
            lastVelocity[i] += c3;
        }
    }

    private void calculateDisplacement(){
        double seconds = lastClock.getSeconds();
        double c1,c2,c3;

        for (int i = 0; i < 3; ++i) { // for 3 axis
            c1 = (double) (lastVelocity[i] + previousVelocity[i]);
            c2 = c1 * seconds;
            c3 = c2 / 2.0;
            lastDisplacement[i] += c3;
        }
    }

    public void resetVelocity(){
        for (int i = 0; i < 3; ++i) {
            previousVelocity[i] = 0;
            lastVelocity[i] = 0;
        }
    }

    public double[] getLastVelocity() {
        return lastVelocity;
    }

    public double[] getLastDisplacement() {
        return lastDisplacement;
    }


    class Clock{
        private long startEventTimeNano;
        private long stopEventTimeNano;
        private boolean isStoped;

        Clock(){
            reset();
        }
        public void stop(){
            isStoped = true;
            stopEventTimeNano = System.nanoTime();
        }

        public void reset(){
            isStoped = false;
            startEventTimeNano = System.nanoTime();
        }
        public double getSeconds(){
            if (isStoped)
                return (stopEventTimeNano - startEventTimeNano) / 1000000000.0;
            else
                return (System.nanoTime() - startEventTimeNano) / 1000000000.0;
        }
    }
}
