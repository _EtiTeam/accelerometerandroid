package eti.asw.accelerometer.accelerometer;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Observable;
import java.util.Observer;


public class Ball extends AppCompatActivity implements Observer {

    Ball thisBall = this;
    BallView view;
    protected Canvas canvas;
    private AccelerometerData accelerometerData;
    final float radius = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view  = new BallView(this);
        canvas = new Canvas();
        setContentView(view);

        accelerometerData = new AccelerometerData(this);
        accelerometerData.addObserver(this);
        this.setTitle("your text");

    }


    public void onReturnButton(View view){
        finish();
        accelerometerData.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        accelerometerData.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        accelerometerData.pause();
    }


    @Override
    public void update(Observable observable, Object o) {
        view.updatePosition();
        view.draw(canvas);
    }

    public class BallView extends View {
        Paint paint = null;
        public BallView(Context context) {
            super(context);
            paint = new Paint();
        }


        @Override
        protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
            super.onLayout(changed, left, top, right, bottom);

            x = new SaturatedFloatValue(getWidth() / 2, 0, getWidth());
            y = new SaturatedFloatValue(getHeight() / 2, 0, getHeight());
        }

        private SaturatedFloatValue x = null;
        private SaturatedFloatValue y = null;

        public void updatePosition(){
            final float multipler = 10;
            float[] data = accelerometerData.getLastGravity();
            x.add(data[0] * -multipler);
            y.add(data[1] * multipler);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.WHITE);
            canvas.drawPaint(paint);
            // Use Color.parseColor to define HTML colors
            paint.setColor(Color.parseColor("#CD5C5C"));
            //canvas.drawCircle(x / 2, y / 2, radius, paint);


            int x = (int)this.x.get();
            int y = (int)this.y.get();
            thisBall.setTitle("x: "+ x+" y: "+y);
            canvas.drawCircle(x, y, radius, paint);
            //canvas.drawCircle(x+60, y, radius, paint);
            invalidate();
        }
    }

    class SaturatedFloatValue{
        float value;
        float maxValue;
        float minValue;

        public SaturatedFloatValue(float currentValue, float minValue, float maxValue){
            this.maxValue = maxValue-radius;
            this.minValue = minValue+radius;
            value = currentValue;
        }

        public void set(float newValue){
            value = newValue;
            if (value > maxValue)
                value = maxValue;
            if (value < minValue)
                value = minValue;
        }

        public void add(float value){
            set(this.value + value);
        }

        public void substract(float value){
            set(this.value - value);
        }

        public float get() {
            return value;
        }
    }
}
